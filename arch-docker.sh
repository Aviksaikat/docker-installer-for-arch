#!/bin/bash

# must run as root man...
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# checking for lolcat
if ! command -v lolcat &> /dev/null
then
    echo "Lolcat not found installing for fun ;)"
    sudo pacman -Sy lolcat --noconfirm
fi

sudo tee /etc/modules-load.d/loop.conf <<< "loop"

# good old updating commands
echo "Updating Packages" | lolcat
sudo pacman -Suyy --noconfirm

# yah finall docker huh..
echo "Installing Docker" | lolcat
sudo pacman -Sy docker --noconfirm

echo "Doing the dirty for you" | lolcat
sudo systemctl start docker.service
sudo systemctl enable docker.service

# addd user to docker group so you don't have to do sudo everytime to use docker. yah it took me a while to know that..
sudo groupadd docker
sudo usermod -aG docker $(whoami || id -u -n)

echo "If anything breaks visit this URL https://linuxhint.com/docker_arch_linux/ (I'm no expert but they are ;)" | lolcat

echo "Now Reboot of you machine will be autorebooted in 20 seceonds. Press Ctrl+c to interrupt" | lolcat

# the sleeep !!
secs=$((1 * 20))
while [ $secs -gt 0 ]; do
   echo -ne "$secs\033[0K\r"
   sleep 1
   : $((secs--))
done

